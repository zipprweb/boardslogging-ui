angular.module('zp_workflow_ui')
  .service('AuthService', function($q, $rootScope, $state, localStorageService) {
  'use strict';

  return {
    getStatus: function() {
      var dfd = $q.defer();
      var name = localStorageService.get('userName');
      if(name !== '' && name !==null) {
        dfd.resolve({
          name: 'Success'
        });
        $rootScope.verifiedType = true;
        $rootScope.LoggedInUser = name;

        $rootScope.showAPReport = (localStorageService.get('workOrder') === 'AP');
        $rootScope.isAdminUser = (localStorageService.get('user_type') === 'admin');
      } else {
        dfd.reject();
        $state.go("login");
      }

      return dfd.promise;
    }
  };
});