angular.module('zp_workflow_ui')
.controller('NavController', function ($rootScope, $scope, $state, ZipprAuthService, localStorageService) {
'use strict';

  $scope.isCollapsed = true;
  $scope.logout =  function(){
    ZipprAuthService.logout().then(function(){
      $rootScope.verifiedType = false;
      $rootScope.LoggedInUser = '';
      $rootScope.showAPReport = false;
      $rootScope.isAdminUser = false;
      clearLocalStorage();
    },
    function(error){
      console.log(error);
      clearLocalStorage();
    });
	};

  $scope.getUserName = function(){
    return localStorageService.get('name') || 'Profile';
  };

  function clearLocalStorage(){
    $scope.isCollapsed = true;
    localStorageService.remove('Id');
    localStorageService.remove('userName');
    localStorageService.remove('sessiontoken');
    localStorageService.remove('apikey');
    localStorageService.remove('clientobjectid');
    localStorageService.remove('client');
    localStorageService.remove('workOrder');
    localStorageService.remove('prefix_code');
    localStorageService.remove('sent_to_print');
    localStorageService.remove('sent_to_installation');
    localStorageService.remove('installed');
    localStorageService.remove('issues');
    localStorageService.remove('buildings');
    localStorageService.remove('parcels');
    localStorageService.remove('converted');
    localStorageService.remove('corner_case');
    localStorageService.remove('total_ddns');
    localStorageService.remove('others');
    localStorageService.remove('ddn_already_exists');
    localStorageService.remove('pending');
    localStorageService.remove('v2ddn_not_found');
    localStorageService.remove('access_road_not_found');

    localStorageService.clearAll();
    $state.go('login');
  }
});