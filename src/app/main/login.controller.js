angular.module('zp_workflow_ui')
.controller('LoginController',function ($rootScope, $scope, $state, $http, ZipprAuthService, $log, localStorageService, logger) {
	'use strict';
  $rootScope.verifiedType = false;
  $scope.loading = false;

  if(localStorageService.get('x-zippr-sessiontoken') !== undefined){
    if(localStorageService.get('user_type') === 'Board_Logger') {
     $rootScope.isAdminUser = true;
    }
    if($rootScope.isAdminUser && !localStorageService.get('workOrder')) {
      $rootScope.showAPReport = (localStorageService.get('workOrder') === 'AP');
      $state.go('home');
    } else {
      $state.go('home');
    }
  }

	$scope.login =  function(){
    if(!$scope.userName || !$scope.password){
      return;
    }

    $scope.errorMsg = '';
    $scope.loading = true;

    ZipprAuthService.login($scope.userName, $scope.password)
      .then(function(data){
        $scope.loading = false;
        if (data.response.user.user_type === 'Board_Logger' || (data.response.user.user_type === 'client' && data.response.user.workOrder)) {
          $rootScope.verifiedType = true;
          localStorageService.set('userName', $scope.userName);
          localStorageService.set('name', data.response.user.name);
          localStorageService.set('user_type', data.response.user.user_type);

          localStorageService.set('x-zippr-sessiontoken', data.response['x-zippr-sessiontoken']);
          $http.defaults.headers.common['x-zippr-sessiontoken'] = data.response['x-zippr-sessiontoken'];

          if(data.response.user.user_type === 'Board_Logger') {
            $rootScope.isAdminUser = true;
            $state.go('home');
          } else {
            $rootScope.isAdminUser = false;
            localStorageService.set('workOrder', data.response.user.workOrder);
            $rootScope.showAPReport = (data.response.user.workOrder === 'AP');
            $state.go('home');
          }
          logger.logSuccess('Logged in successfully.');
        } else {
          $scope.errorMsg = 'You do not have permission to login!!';
        }
      }, function(error){
        $scope.password = '';
        $scope.loading = false;
        $scope.errorMsg = (error && error.error && error.error.reason) ? error.error.reason : 'Invalid Username or Password.';
        $log.debug(error,'error');
      });
    };
});