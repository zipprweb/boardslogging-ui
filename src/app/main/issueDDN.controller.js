angular.module('zp_workflow_ui')
.controller('issueDDNController',function ($rootScope, logger, $scope, $window,$http, ZipprAPI, uiGridConstants, $uibModal, localStorageService, $interval) {
	
	$scope.issueddn = '';
	$scope.updateissueddn = function(){
		$scope.statusForDisplay = '';
		if(!$scope.issueddn){
			logger.logError('Please Enter the DDN');
		}
		else{
			var query = {
				"ddn":$scope.issueddn
			};
			ZipprAPI.updateIssueDDNLog.update(query, function(data) {
				console.log(data.response);
				if(data.ok === true){
					if(data.response.updation){
						//alert(data.response.updation)
						$scope.statusForDisplay = 'Status: ' + data.response.updation.toString(); 
						/*var bundle_details = data.response.bundle_details.bundle.bundleNo
						var modalInstance1 = $uibModal.open({
						templateUrl :'app/partials/modals/modalDisplayBundleNo.html',
						controller: 'DisplayBundleNoModalController',
						size: 'md',
						backdrop: 'static',
						keyboard: false, 
						resolve :{
							bundle_details:function(){
								return bundle_details;
							}
						}
						});
						modalInstance1.result.then(function(data) {
							console.log('I am on closing wth data', data);
						}, function () {
							console.log('Modal dismissed at: ' + new Date());
						});	*/
					}
					else{
						logger.logError('Data Not Found');
					}	
				}
				else{
					logger.logError('Error: '+data.error.reason);
				}
		});
		}
	}
});
 