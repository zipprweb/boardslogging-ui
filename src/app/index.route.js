(function() {
	'use strict';

	angular
		.module('zp_workflow_ui')
		.config(routeConfig);

	/** @ngInject */
	function routeConfig($stateProvider, $urlRouterProvider) {
		$stateProvider
		.state('login', {
			url: '/login',
			cache: false,
			templateUrl: 'app/partials/login.html',
			controller: 'LoginController'
		})
		.state('home', {
			url:'/home',
			templateUrl:'app/partials/ddn.html',
			controller:'issueDDNController',
			resolve:{
				status: function(AuthService){
					return AuthService.getStatus();
				}
			}
		})
		$urlRouterProvider.otherwise('/login');
	}
})();