(function() {
  'use strict';

  angular
    .module('zp_workflow_ui', 
      [
      'ngCookies',
      'ngTouch',
      'ngResource',
      'ngSanitize',
      'ui.bootstrap',
      'ui.router',
      'zippr.config',
      'zippr.auth',
      'zippr.api',
      'LocalStorageModule',
      'app.ui.services',
      'ngAnimate',
      'nvd3',
      'ui.grid',
      'ui.grid.exporter',
      'treeGrid'
      ]);
})();