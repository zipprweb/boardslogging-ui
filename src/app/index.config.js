(function() {
  'use strict';

  angular
    .module('zp_workflow_ui')
    .constant('USER_ROLES', {
      admin: 'admin',
      public: 'public'
    })
    .config(config);

  /** @ngInject */
  function config($logProvider, localStorageServiceProvider) {
    // Enable log
    $logProvider.debugEnabled(true);
    localStorageServiceProvider.setPrefix('zp_workflow_ui').setStorageType('sessionStorage').setNotify(true, true);
  }
})();

(function() {
  'use strict';
  angular
    .module('zp_workflow_ui')
    .config(['$compileProvider', function ($compileProvider) {
      $compileProvider.aHrefSanitizationWhitelist(/^\s*(|blob|https|):/);
  }]);
})();