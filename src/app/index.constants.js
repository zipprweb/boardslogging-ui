(function() {
  'use strict';

  angular
    .module('zp_workflow_ui')
    .constant('zippr_server', 'http://qa.zip.pr/');
})();
